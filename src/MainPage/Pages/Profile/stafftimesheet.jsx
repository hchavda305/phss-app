/**
 * TermsCondition Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16 } from '../../../Entryfile/imagepath'

export default class EmployeeProfile extends Component {
  render() {
    return (
        <div className="page-wrapper">
            <Helmet>
              <title>Employee Profile - PARTICIPATION HOUSE SUPPORT SERVICES</title>
              <meta name="description" content="Reactify Blank Page" />
            </Helmet>

            {/* Page Content */}
            <div className="content container-fluid">
              {/* Page Header */}

              <div className="page-header">
                <div className="row">
                  <div className="col-sm-4">
                    <h5 className="page-title"><small className="text-info">Marshall Dunn</small> ID:0100</h5>
                  </div>
                  <div className="col-sm-4">
                    <h5 className="page-title">Role: <small className="text-info">Staff Method</small></h5>
                  </div>
                  <div className="col-sm-4">
                    <h5 className="page-title">Primary Location: <small className="text-info">Belgrave</small></h5>
                  </div>
                </div>
              </div>

              <div className="page-header">
                <div className="row">
                  <div className="col-sm-12">
                    <h3 className="page-title">Time Sheet</h3>
                    <ul className="breadcrumb">
                      <li className="breadcrumb-item"><a href="/app/main/dashboard">Dashboard</a></li>
                      <li className="breadcrumb-item active">Time Sheet</li>
                    </ul>
                  </div>
                </div>
              </div>
              {/* /Page Header */}
              <div className="card tab-box">
                <div className="row user-tabs">
                  <div className="col-lg-12 col-md-12 col-sm-12 line-tabs">
                    <ul className="nav nav-tabs nav-tabs-bottom">
                      <li className="nav-item"><a href="#current_time_staff_tab" data-toggle="tab" className="nav-link active">Current Time Staff</a></li>
                      <li className="nav-item"><a href="#past_time_staff_tab" data-toggle="tab" className="nav-link">Past Time Staff</a></li>
                      
                    </ul>
                  </div>
                </div>
              </div>
              <div className="card mb-0">
                <div className="card-body">
                  <div className="row">
                    <div className="col-md-12">
                      <h3 className="user-name m-t-0 mb-0">Time Sheet</h3>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12">
                      <div className="profile-view">
                        <div className="profile-basic">
                          <div className="row">
                            <div className="col-md-4">
                              <div className="profile-info-left">
                                <h4 className="user-name m-t-0 mb-0">Tom Cruise</h4>
                                <h4 className="user-name m-t-0 mb-0">ID 8765-98</h4>
                              </div>
                            </div>
                            <div className="col-md-4">
                              <div className="profile-info-left">
                                <div className="staff-id">Scheduled hours : 40h 00 min</div>
                                <div className="staff-id">Worked Hours : 28h 15 min</div>
                              </div>
                            </div>
                            <div className="col-md-4">
                              <div className="profile-info-left">
                                <div className="staff-id">Time Sheet Due by : Feb 05 at 4:00 PM</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-4">
                      <h3 className="user-name m-t-0 mb-0">Active Time Sheet</h3>
                    </div>
                    <div className="col-md-2">
                      <h3 className="user-name m-t-0 mb-0">Back</h3>
                    </div>
                    <div className="col-md-4">
                      <h3 className="user-name m-t-0 mb-0">Period 01-24-21 to 02-06-21</h3>
                    </div>
                    <div className="col-md-2">
                      <h3 className="user-name m-t-0 mb-0">For</h3>
                    </div>
                  </div>
                </div>
              </div>
              

              <div className="tab-content">
                {/* //============ Current Time Staff Tab ============ */}
                <div id="current_time_staff_tab" className="pro-overview tab-pane fade show active">
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Current Time Sheet<a href="#" className="edit-icon" data-toggle="modal" data-target="#CurrentTimeStaff_modal"><i className="fa fa-pencil" /></a></h3>
                          <div className="table-responsive">
                            <table className="table table-nowrap">
                              <thead>
                                <tr>
                                  <th>Service Name</th>
                                  <th>Date</th>
                                  <th>Time In</th>
                                  <th>Time Out</th>
                                  <th>Location</th>
                                  <th>Note</th>
                                  <th>N# of hours</th>
                                  <th>Action</th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Regular</td>
                                  <td>01-25-21</td>
                                  <td>8:00 am</td>
                                  <td>12:00 pm</td>
                                  <td>Cranbrook</td>
                                  <td></td>
                                  <td>4h</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>TRIP-24 Hour</td>
                                  <td>01-26-21</td>
                                  <td>8:00 am</td>
                                  <td>12:00 pm</td>
                                  <td>Cranbrook</td>
                                  <td></td>
                                  <td>4h</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Asleep</td>
                                  <td>01-27-21</td>
                                  <td>9:00 am</td>
                                  <td>-</td>
                                  <td>Belgrave</td>
                                  <td></td>
                                  <td>3h</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Asleep</td>
                                  <td>01-27-22</td>
                                  <td>-</td>
                                  <td>7:00 am</td>
                                  <td>Belgrave</td>
                                  <td></td>
                                  <td>7h</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Hospital</td>
                                  <td>01-28-21</td>
                                  <td>1:00 pm</td>
                                  <td>3:15 pm</td>
                                  <td>Cranbrook</td>
                                  <td></td>
                                  <td>3h 15 min</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Facilitation</td>
                                  <td>01-28-21</td>
                                  <td>2:00 pm</td>
                                  <td>7:00 pm</td>
                                  <td>Cranbrook</td>
                                  <td></td>
                                  <td>5h</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Sick time</td>
                                  <td>01-29-21</td>
                                  <td>2:00 pm</td>
                                  <td>7:00 pm</td>
                                  <td>Cranbrook</td>
                                  <td></td>
                                  <td>5h</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
                                        <a href="#" className="dropdown-item"><i className="fa fa-trash-o m-r-5" /> Delete</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //============ Current Time Staff Tab ============ */}

                {/* //============ Past Time Staff Tab ============ */}
                <div id="past_time_staff_tab" className="tab-pane fade">
                  <div className="row">
                    <div className="col-md-12 d-flex">
                      <div className="card profile-box flex-fill">
                        <div className="card-body">
                          <h3 className="card-title">Past TimeSheets<a href="#" className="edit-icon" data-toggle="modal" data-target="#CurrentTimeStaff_modal"><i className="fa fa-pencil" /></a></h3>
                          <div className="table-responsive">
                            <table className="table table-nowrap">
                              <thead>
                                <tr>
                                  <th>Pay Period</th>
                                  <th>Total hours</th>
                                  <th>Approval By</th>
                                  <th>Action</th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Nov 01 2020 to Nov 14 2020</td>
                                  <td>79h 00min</td>
                                  <td>Adele</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-eye m-r-5" /> View</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Nov 15 2020 to Nov 28 2020</td>
                                  <td>80h 00min</td>
                                  <td>Leslie</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-eye m-r-5" /> View</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Nov 29 2020 to Dec 12 2020</td>
                                  <td>80h 00min</td>
                                  <td>Adele</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-eye m-r-5" /> View</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Dec 13 2020 to Dec 16 2020</td>
                                  <td>79h 00min</td>
                                  <td>Adele</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-eye m-r-5" /> View</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Dec 13 2020 to Jan 9 2020</td>
                                  <td>81h 00min</td>
                                  <td>Adele</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-eye m-r-5" /> View</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Jan 10 2021 to Jan 23 2020</td>
                                  <td>33h:15min</td>
                                  <td>Adele</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-eye m-r-5" /> View</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Jan 24 2021 to Feb 24 2020</td>
                                  <td>33h:15min</td>
                                  <td>Adele</td>
                                  <td className="text-right">
                                    <div className="dropdown dropdown-action">
                                      <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
                                      <div className="dropdown-menu dropdown-menu-right">
                                        <a href="#" className="dropdown-item"><i className="fa fa-eye m-r-5" /> View</a>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                {/* //============ Past Time Staff Tab ============ */}
              </div>
            </div>
            {/* //Page Content */}


            {/* ------------------------- Modes ------------------------- */}

            {/* ****************** Current Time Staff Tab Modals ****************** */}
            {/* Current Time Staff Modal */}
            <div id="CurrentTimeStaff_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Current Time Staff</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>Service Name<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>CPI Training 1</option>
                                  <option>CPI Training 2</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>Location<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Cranbrook 1</option>
                                  <option>Cranbrook 2</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>Reason<span className="text-danger">*</span></label>
                                <select className="select form-control">
                                  <option>-</option>
                                  <option>Reason 1</option>
                                  <option>Reason 2</option>
                                </select>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Start date<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>End date<span className="text-danger">*</span></label>
                                <div className="cal-icon">
                                  <input className="form-control datetimepicker" type="text" />
                                </div>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Time in<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Time out<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                            <div className="col-md-12">
                              <div className="form-group">
                                <label>Notes<span className="text-danger">*</span></label>
                                <input className="form-control" type="text" />
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn">Save</button>
                            <button className="btn btn-primary submit-btn">Submit to approval</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            {/* //Current Time Staff Modal */}
            {/* ****************** Current Time Staff Tab Modals ****************** */}

            {/* ------------------------- Modes ------------------------- */}
          </div>
       
    );
  }
}
