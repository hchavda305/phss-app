/**
 * Tables Routes
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

// mct route
import StaffProfile from "./staffprofile"
// mct route


import EmployeeProfile from "./employeeprofile"
import ClientProfile from "./clientprofile"



const subscriptionroute = ({ match }) => (
    <Switch>
        <Redirect exact from={`${match.url}/`} to={`${match.url}/employee-profile`} />
        <Route path={`${match.url}/employee-profile`} component={EmployeeProfile} />
        <Route path={`${match.url}/client-profile`} component={ClientProfile} />

        <Route path={`${match.url}/staff-profile`} component={StaffProfile} />
        
    </Switch>
);

export default subscriptionroute;
