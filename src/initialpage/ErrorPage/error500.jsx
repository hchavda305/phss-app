/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";

class Error500 extends Component {
    componentDidMount(){
        $('body').addClass('error-page');
    }
    render() {
     
      return (
        <div className="main-wrapper">
            <Helmet>
                <title>Error 500 - PARTICIPATION HOUSE SUPPORT SERVICES</title>
                <meta name="description" content="Login page"/>					
            </Helmet>
        <div className="error-box">
          <h1>500</h1>
          <h3><i className="fa fa-warning" /> Oops! Something went wrong</h3>
          <p>The page you requested was not found.</p>
          <a onClick={()=>localStorage.setItem("firstload","true")} href="/login" className="btn btn-custom">Back to Login</a>
        </div>
      </div>

        
      );
   }
}

export default Error500;
