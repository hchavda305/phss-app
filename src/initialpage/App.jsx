import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
// We will create these two pages in a moment
//Authendication


import RegistrationPage from './Notuse/RegistrationPage';
import LockScreen from './Notuse//lockscreen'
import SecurityQuestions from './Notuse/securityquestions'
import ForgotSecurityQuestions from './Notuse/forgotsecurityquestions'
import MobileSubmit from './Notuse/mobilesubmit'

import ApplyJobs from './ApplyJob';
import OTP from './otp';
import ForgotOTP from './Notuse/forgototp';
import ResetPassword from './Notuse/resetpassword';



//MCT
import LoginPage from './loginpage';
import ForgotPassword from './forgotpassword';
import AccountVerification from './accountverification';
import StaffProfile from "./PHSSPage/staffprofile";
import Dashboard from './PHSSPage/dashboard';
import Settings from './PHSSPage/settings';
import Mytimesheet from './PHSSPage/timesheet';
import AttendanceSummary from './PHSSPage/attendancesummary';
import Employees from './PHSSPage/employees';
import EmployeeProfile from "./PHSSPage/employeeprofile";
import ChangePassword from './PHSSPage/changepassword';
import Location from "./PHSSPage/location";
import Holiday from "./PHSSPage/holiday";
import Notifications from "./PHSSPage/notifications";
import Reports from "./PHSSPage/reports";
import ConfirmPersonalProfile from "./PHSSPage/confirmpersonalprofile";
import Schedule from "./PHSSPage/schedule";
import Availability from "./PHSSPage/profile/Availability";
import Entitlements from "./PHSSPage/Entitlements";
import BacthPayrollReports from "./PHSSPage/bacthpayrollreports";

import PayrollAdjustments from "./PHSSPage/payrolladjustments";
import AuditReports from "./PHSSPage/auditreports";
import PayrollAdjustmentsAdd from "./PHSSPage/payrolladjustmentsadd";
//MCT













//Main App
// import DefaultLayout from './Sidebar/DefaultLayout';
// import Settinglayout from './Sidebar/Settinglayout';
//import Tasklayout from './Sidebar/tasklayout';
//import Emaillayout from './Sidebar/emaillayout';
//import chatlayout from './Sidebar/chatlayout';

// import uicomponents from '../MainPage/UIinterface/components';
//Error Page
import Error404 from './ErrorPage/error404';
import Error500 from './ErrorPage/error500';


// import 'Assets/css/font-awesome.min.css';

import $ from 'jquery';
// window.jQuery = $;
// window.$ = $;
// import UserPage from './pages/UserPage'
/**
 * Initial Path To Check Whether User Is Logged In Or Not
 */
// const InitialPath = ({ component: Component, ...rest, authUser }) =>
//    <Route
//       {...rest}
//       render={props =>
//          authUser
//             ? <Component {...props} />
//             : <Redirect
//                to={{
//                   pathname: '/login',
//                   state: { from: props.location }
//                }}
//             />}
//    />;

export default class App extends Component {
    componentDidMount(){
        if (location.pathname.includes("login") || location.pathname.includes("register") || location.pathname.includes("forgotpassword")
        || location.pathname.includes("resetpassword") || location.pathname.includes("mobilesubmit") || location.pathname.includes("otp")|| location.pathname.includes("lockscreen")  ) {
            $('body').addClass('account-page');
        }else if (location.pathname.includes("error-404") || location.pathname.includes("error-500") ) {
            $('body').addClass('error-page');
        }
        
    } 
       render(){
            const { location, match, user } = this.props;
            
            
            // if (location.pathname === '/') {
            // if (user === null) {
            //     return (<Redirect to={'/login'} />);
            // } else {
            //     return (<Redirect to={'/app/main/dashboard'} />);
            // }
            // }
            if (location.pathname === '/') {
                 
                   //return (<Redirect to={'/app/main/dashboard'} />);
                   return (<Redirect to={'/login'} />);
                
            }
            
            return (
                <Switch>
                    {/* <InitialPath
                        path={`${match.url}app`}
                        // authUser={user}
                        component={DefaultLayout}
                    /> */}
                    {/* <Redirect exact from={`${match.url}/`} to={`${match.url}/login`} /> */}
                    <Route path="/login" component={LoginPage} />
                    <Route path="/OTPVerification" component={OTP} />
                    <Route path="/forgot-password" component={ForgotPassword} />
                    <Route path="/account_verification/:id" component={AccountVerification} />

                {/* NOT USER ROUTE */}
                    <Route path="/setusername" component={MobileSubmit} />
                    <Route path="/forgototp" component={ForgotOTP} />
                    <Route path="/securityquestions" component={SecurityQuestions} />
                    <Route path="/forgotsecurityquestions" component={ForgotSecurityQuestions} />
                    <Route path="/resetpassword" component={ResetPassword} />

                    
                {/* NOT USER ROUTE */}    
                    
                    
                    
                    
                    

                    <Route path="/dashboard" component={Dashboard} />
                    <Route path="/staff-profile" component={StaffProfile} />
                    <Route path="/settings" component={Settings} />
                    <Route path="/my-timesheet" component={Mytimesheet} />
                    <Route path="/employees" component={Employees} />
                    <Route path="/employee-profile/:id" component={EmployeeProfile} />
                    <Route path="/change-password" component={ChangePassword} />
                    <Route path="/location" component={Location} />
                    <Route path="/attendance-summary" component={AttendanceSummary} />

                    <Route path="/holiday" component={Holiday} />
                    <Route path="/notifications" component={Notifications} />
                    <Route path="/reports" component={Reports} />
                    <Route path="/confirm-profile" component={ConfirmPersonalProfile} />
                    <Route path="/schedule" component={Schedule} />
                    <Route path="/availability" component={Availability} />
                    <Route path="/entitlements" component={Entitlements} />
                    <Route path="/batchpayrollreports" component={BacthPayrollReports} />
                    
                    <Route path="/payrolladjustments" component={PayrollAdjustments} />
                    <Route path="/auditreports" component={AuditReports} />
                    
                    <Route path="/payrolladjustmentsadd" component={PayrollAdjustmentsAdd} />

                {/*
                    <Route path="/register" component={RegistrationPage} />
                    <Route path="/lockscreen" component={LockScreen} />
                    <Route path="/applyjob" component={ApplyJobs} />
                    <Route path="/settings" component={Settinglayout} />
                    <Route path="/tasks" component={Tasklayout} />
                    <Route path="/email" component={Emaillayout} />
                    <Route path="/conversation" component={chatlayout} />
                    <Route path="/ui-components" component={uicomponents} />
                    <Route path="/app" component={DefaultLayout} />
                */}
                    
                    
                    

                    
                    
                    
                    <Route path="/error-400" component={Error404} />
                    <Route path="/error-500" component={Error500} />
                </Switch>
            )
        }
         
}
